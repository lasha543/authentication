package com.example.authentication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_signup.*

class loginActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        init()
    }

    private fun init(){
        auth = Firebase.auth
        logInButton.setOnClickListener{
            login()


        }



    }
    private fun login(){
        val email = emailMadeText.text.toString()
        val password = passwordMadeText.text.toString()

        if(email.isNotEmpty() && password.isNotEmpty()){
            progressBar2.visibility=View.VISIBLE
            logInButton.isClickable=false
            auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    progressBar2.visibility=View.INVISIBLE
                    logInButton.isClickable=true
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d("login", "signInWithEmail:success")
                        val user = auth.currentUser
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.d("login", "signInWithEmail:failure", task.exception)
                        Toast.makeText(baseContext, "Email format is not Correct",
                            Toast.LENGTH_SHORT).show()
                    }

                }




        }else{
            Toast.makeText(this,"Please Fill all Fields",Toast.LENGTH_SHORT).show()
        }




    }





}